# README #

* Note:  the project TODOs are in the bottom of this document

### What is this repository for? ###

This is an attempt to have a neural net create a voxel STL 3D model based on an image.
I use ThreeJS, load 3D models, and get images using a ThreeJS camera.  Then I feed that to the neural net, followed by loading the output layer into the ThreeJS scene assuming it's a voxel based STL file.
I get the error based on difference in the camera image of that from the input image and backprop, rinse and repeat.

* Ultimately, this is also an attempt to train a model which can do the same thing with objects in real life from a cheap webcam, creating voxel-based STL 3D models from them.  It would do no good to stay in the game environment as you have to have 3D models to begin with.

* Note that the output of the main thrust of this project, the trained neural net model, will be a voxel-based 3D model in STL format.  It would then be possible to add another neural net later to classify the object, and this could be assisted by further code using ThreeJS to move a camera around the object, feeding the images to the CNN classifier.

* Version
  0.0.1

### How do I get set up? ###

* You'll need a simple web server, you could use VScode with the Live Server extension (for the browser-based ThreeJS).
  The easiest way to do the Python is to install Anaconda, I intend to include an environment file and a readme with commands to easily instantiate your Python environment and start the scripts (which will run the neural net using
  Keras with Tensorflow backend).
* Configuration:  Install Anaconda, gather 3D models (stl format), get a webserver (I used the VSCode Live Server extension)
* Dependencies: ThreeJS and a webserver, Anaconda
* Database configuration:  No database - the 3D models are all that is needed, these are stl files.
* How to run tests: 
* Deployment instructions: Clone the repository and leave the folders as-is.  Start the webserver from the directory with html	files, do the Anaconda environment commands and python script start commands in the directory with the python scripts,
  and put a bunch of 3D models (whatever you like) in the directory for models.  I will make changes to these as I go.

### Contribution guidelines ###

* Writing tests - could be code to pull down 3D model datasets into the input models folder
* Code review - I won't pull in code that doesn't work.  Feel free to submit code pulls with just comments and questions though
* Other guidelines - feel free to make suggestions on any changes to the neural net model or suggest resources, etc

### Who do I talk to? ###

* Put a pull request in and feel free to include files with comments / questions
* There are no other contacts yet, will change this if anyone else gets involved and wants to be a contact.

### Project TODO steps / reference listing ###

* DONE:  Get a basic ThreeJS environment going.  Note:  I used this vid, might sign up for that advertised set of classes
			https://www.youtube.com/watch?v=6oFvqLfRnsU

* DONE:  Add code for loading 3D models (stl format):
			https://stackoverflow.com/questions/11060261/what-is-the-preferred-method-for-loading-stl-files-in-three-js
			https://threejs.org/examples/webgl_loader_stl.html  (opened dev tools and read through source, also grabbed	the javascript file with the stl loader

* TODO Add code for switching camera between source 3D model and generated 3D model in ThreeJS scene.

* TODO Add code to spin the camera around the input model, making sure the camera around generated model matches the position relative to the model (for neural net training - it'll be supervised training using this approach)

* TODO Gather STL files and upt them in a folder - if I do not point at a open source stl dataset, this will be left to everyone cloning this repo to do.  However, there may already be stl 3D model datasets out there that were made for training neural nets, if so, should make cloning / loading code in this step for one of those datasets
		If we're gathering stl files, https://thingiverse.com

* TODO Make javascript code to cycle through the 3dmodels folder and load them one by one into the scene.
		Also in this step, add AJAX code to communicate with the python services to know when they are done / what the name of the output stl file is.  Note:  might be able to just watch the directories for new files as an alternative approach here (essentially using the hard disk as the communication between the ThreeJS/javascript and python service).  There are a lot of ways to accomplish this, the python service could also watch the screen like this:  https://github.com/Sentdex/pygta5/blob/master/grabscreen.py
		I might then need two ThreeJS environments so I could have two cameras, one for input model and one for output.

* TODO Make javascript code to save the image from the ThreeJS camera of the input & generated models in a folder where	the python service can pick it up.
		
* TODO Make new Anaconda environment with all dependencies required for the service.  Save the environment description file to this repo and make a readme file with commands to use the environment file to make the environment.

* TODO Setup neural net model using Keras with Tensorflow backend

* TODO Make image loader and put the pixel data in neural net input layer (numpy ndarray - see sentdex and Adrian Rosebrock example code)

* TODO Assume output layer is a voxel array, make function to generate STL file from it (first two for reference to	better understand the file format only):
		http://www.fabbers.com/tech/STL_Format
		https://danbscott.ghost.io/writing-an-stl-file-from-scratch/
        https://3dprinting.stackexchange.com/questions/10205/convert-a-3d-numpy-array-of-voxels-to-an-stl-file
		https://pypi.org/project/numpy-stl/

* TODO Write (Keras will have example docs, this is likely just a line or two) python code to forward propagate the input image

* TODO Change the output layer (or otherwise change the model) to allow backprop of the image from the camera in ThreeJS looking at the output 3D model from this neural net.  I'm thinking about not bothering with error on the stl model portion of the output layer, but rather just getting a difference between camera images of the	input and output models.  This is tricky, and I may have to do something completely non-standard, like maybe a threeway layer connection or something like that, though maybe GANs would provide inspiration.

* Write backprop python code for whatever model is setup in step 13 above.

* Testing and reiteration - if the above isn't working at all, go back to the drawing board.  The inspiration for this project was this paper:  https://eng.ucmerced.edu/people/jyang44/papers/nips16_ptn.pdf
	In that project, they send multiple images of the input model at 30 degree angles between camera takes. They also do something that I didn't quite understand how to do - sending the silohuettes (maybe that's what they're sending instead of images).